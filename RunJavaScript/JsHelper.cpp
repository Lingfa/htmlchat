#include "MainWindow.h"
#include <QDebug>
#include "JsHelper.h"

JsHelper::JsHelper(MainWindow *win, QObject *parent)
    : QObject(parent)
    , m_win(win)
{
}

// This slot is invoked from the HTML client side and the text displayed on the c++ server side.
void JsHelper::receiveText(const QString &text)
{
    emit m_win->message(text);
}

void JsHelper::validateForm(const QVariant &v)
{
    m_win->validateForm(v);
}
