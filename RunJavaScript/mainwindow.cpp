#include <QWebEngineView>
#include <QPushButton>
#include <QToolBar>
#include <QMessageBox>
#include <QFile>
#include <QWebChannel>
#include <QDockWidget>
#include <QTextBrowser>
#include "JsHelper.h"
#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_view(new QWebEngineView(this))
    , m_jsHelper(new JsHelper(this, this))
    , m_htmlObjectName("jsHelper")
{
    setCentralWidget(m_view);
    connect(m_view, SIGNAL(loadFinished(bool)), this, SLOT(injectJavaScript()));

    QTextBrowser *browser = new QTextBrowser(this);
    connect(this, SIGNAL(message(QString)), browser, SLOT(append(QString)));

    QDockWidget *dock = new QDockWidget(this);
    dock->setWidget(browser);
    addDockWidget(Qt::BottomDockWidgetArea, dock);

    QString html =  QString("<html>"
                            "<head>"
                            "<body>"
                            "<h1>HTML page</h1>"
                            "Press ToolBar button to test JavaScript function call from C++, or"
                            "<br/><br/><br/>"
                            "<button onclick='testHtmlCallCpp()'>Test HTML call C++ </button>"
                            "<script>"
                            "    function testHtmlCallCpp() {"
                            "      %1.receiveText('Hello C++');"
                            "    }"
                            "</script>"
                            "<br/><br/><br/>"
                            "<form name='myForm' onsubmit=\"%1.validateForm(document.forms['myForm']['fname'].value)\" >"
                            "  Name: <input type='text' name='fname' /><input type='submit' value='Submit' />"
                            "</form>"
                            "</body>"
                            "</html>").arg(m_htmlObjectName);
    m_view->setHtml(html);

    QPushButton *onePlusTwoButton = new QPushButton("1+2");
    connect(onePlusTwoButton, SIGNAL(clicked(bool)), this, SLOT(onePlusTwo()));

    QToolBar *bar = this->addToolBar("ToolBar");
    bar->addWidget(onePlusTwoButton);

    QAction *a = new QAction("CppToHtml");
    connect(a, SIGNAL(triggered(bool)), this, SLOT(testCppToHtmlCall()));
    bar->addAction(a);

    a = new QAction("RetraveFormName");
    connect(a, SIGNAL(triggered(bool)), this, SLOT(retraveFormName()));
    bar->addAction(a);
}

MainWindow::~MainWindow()
{

}

void MainWindow::injectJavaScript()
{
    QString script = "function plus(a,b) {return a+b;}";
    m_view->page()->runJavaScript(script, [&] (const QVariant &) {
        injectWebChannelJavaScript();
    });
}

void MainWindow::onePlusTwo()
{
    QString script = "plus(1,2)";
    m_view->page()->runJavaScript(script, [&] (const QVariant &v) {
        QMessageBox::information(this, "Result", v.toString()) ;
    });
}

void MainWindow::injectWebChannelJavaScript()
{
    QString fileName = ":/qwebchannel.js";
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        emit message("Cannot open" + fileName);
        return;
    }

    QTextStream in(&file);
    QString js = in.readAll();
    file.close();

    emit message("Start injectWebChannelJavaScript() from file:" + fileName);
    m_view->page()->runJavaScript(js, [&, fileName] (const QVariant &) {
        emit message("Finished injectWebChannelJavaScript() from file:" + fileName);
        createWenChannels();
    });
    emit message("injecting ...");
}

void MainWindow::createWenChannels()
{
    QWebChannel *channel = new QWebChannel(this);
    channel->registerObject(m_htmlObjectName, m_jsHelper);
    m_view->page()->setWebChannel(channel);


    QString js = QString("new QWebChannel(qt.webChannelTransport, function(channel) {"
                         "    window.%1 = channel.objects.%1;"
                         "    window.%1.sendText.connect(function(message) {"
                         "        alert('Received C++ %1 message: ' + message)"
                         "    });"
                         "});").arg(m_htmlObjectName);

    emit message("Start JavaScript new QWebChannel");
    m_view->page()->runJavaScript(js, [&](const QVariant &) {
        emit message(" Finished.");
    });
    emit message(" running ...");
}

void MainWindow::testCppToHtmlCall()
{
    emit m_jsHelper->sendText("Cpp says hello to HTML");
}

void MainWindow::retraveFormName()
{
    QString js = "document.forms['myForm']['fname'].value";
    m_view->page()->runJavaScript(js, [&](const QVariant &v) {
        emit message(v.toString());
    });
}

void MainWindow::validateForm(const QVariant &v)
{
    QString s = v.toString();
    if (s.isEmpty()) {
        s += "(EMPTY)";
    }
    emit message("Message received from HTML:" + s);
}
