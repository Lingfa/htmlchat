#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QWebEngineView;
class JsHelper;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void validateForm(const QVariant &v);
signals:
    void message(const QString &) const;
private slots:
    void onePlusTwo();
    void injectJavaScript();
    void testCppToHtmlCall();
    void retraveFormName();
private:
    QWebEngineView *m_view;
    void injectWebChannelJavaScript();
    void createWenChannels();
    JsHelper *m_jsHelper;
    QString m_htmlObjectName;
};

#endif // MAINWINDOW_H
