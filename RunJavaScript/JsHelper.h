#ifndef JSHELPER_H
#define JSHELPER_H

#include <QObject>

class MainWindow;

class JsHelper : public QObject
{
    Q_OBJECT
public:
    JsHelper(MainWindow *win, QObject *parent = nullptr);
signals:
    void sendText(const QString &text);
public slots:
    void receiveText(const QString &text);
    void validateForm(const QVariant &v);
private:
    MainWindow *m_win;
};

#endif // JSHELPER_H
