#include "MainWindow.h"
#include "JsHelper.h"

JsHelper::JsHelper(MainWindow *win, QObject *parent)
    : QObject(parent)
    , m_win(win)
{
}

// This slot is invoked from the HTML client side and the text displayed on the c++ server side.
void JsHelper::receiveText(const QString &text)
{
    //m_win->displayMessage("Received message: " + text);
    //return text;
}
