#ifndef WEBSOCKETTRANSPORT_H
#define WEBSOCKETTRANSPORT_H


#include <QWebChannelAbstractTransport> // since 5.4, QT += webchannel

QT_BEGIN_NAMESPACE
class QWebSocket;
QT_END_NAMESPACE

class WebSocketTransport : public QWebChannelAbstractTransport
{
    Q_OBJECT
public:
    explicit WebSocketTransport(QWebSocket *socket);
    virtual ~WebSocketTransport();

    void sendMessage(const QJsonObject &message) override;
    void sendPlainTextMessage(const QString &text);
    bool isValid() const;
    QWebSocket *socket() const {return m_socket;}
private slots:
    void textMessageReceived(const QString &message);

private:
    QWebSocket *m_socket;
};

#endif // WEBSOCKETTRANSPORT_H