#include <QTextBrowser>
#include <QStatusBar>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QFileInfo>
#include <QDir>
#include <QMessageBox>
#include <QWebSocketServer> // require QT += websockets
#include <QWebSocket>
#include <QWebChannel>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrl>
#include <QDesktopServices>
#include "WebSocketTransport.h"
#include "JsHelper.h"
#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_browser(new QTextBrowser(this))
    , m_jsHelper(new JsHelper(this,this))
    , m_lineEdit(new QLineEdit(this))
    , m_server(nullptr)
    , m_channel(nullptr)
    , m_currentTransport(nullptr)
    , m_clients(new QComboBox(this))
{
    setCentralWidget(m_browser);
    resize(680, 540);

    createBar();

    createWebSocket();
    connect(m_clients, SIGNAL(currentIndexChanged(int)), this, SLOT(clientChanged(int)));
}

MainWindow::~MainWindow()
{

}

void MainWindow::displayMessage(const QString &message)
{
    m_browser->append(message);
}


void MainWindow::createBar()
{
    QStatusBar *bar = statusBar();
    QPushButton *clearButton = new QPushButton("Clear");
    QPushButton *sendButton = new QPushButton("Send");
    bar->addPermanentWidget(clearButton);
    bar->addPermanentWidget(m_lineEdit, 8);
    bar->addPermanentWidget(sendButton);
    bar->addPermanentWidget(m_clients);

    m_browser->append("Type message and click \"Send\" button.");

    clearButton->setToolTip("Clear both browser and lineEdit text.");
    connect(clearButton, SIGNAL(clicked(bool)), m_browser, SLOT(clear()));
    connect(clearButton, SIGNAL(clicked(bool)), m_lineEdit, SLOT(clear()));

    connect(sendButton, SIGNAL(clicked(bool)), this, SLOT(sendMessage()));
}

void MainWindow::createWebSocket()
{
    QString path = QDir::currentPath();
    QString name = "qwebchannel.js";
    copyTo(":/res/" + name, path + "/" + name); // if does not exist
    name = "index.html";
    if (!copyTo(":/res/" + name, path + "/" + name, true)) { // force to overwrite if exists
        qDebug() << "Fail to copy file " + name;
        return;
    }

    // setup the QWebSocketServer
    m_server = new QWebSocketServer("WebSocket app", QWebSocketServer::NonSecureMode, this);
    if (!m_server->listen(QHostAddress::LocalHost, 12345)) {
        qFatal("Failed to open web socket server, probabaly caused by an existing instance!");
        return;
    }

    // setup the channel
    m_channel = new QWebChannel(this);

    // setup the core and publish it to the QWebChannel
    m_channel->registerObject(QStringLiteral("core"), m_jsHelper);

    connect(m_server, SIGNAL(newConnection()), this, SLOT(newConnection()));


    // open two browser windows with the same client HTML page. This requires client must has *.html open by browser
    QUrl url = QUrl::fromLocalFile(BUILD_DIR"/index.html");
    QDesktopServices::openUrl(url);
    QDesktopServices::openUrl(url);

    m_browser->setOpenLinks(false);
    connect(m_browser, SIGNAL(anchorClicked(QUrl)), this, SLOT(anchorClicked(QUrl)));
    m_browser->append(tr("Initialization complete, opening two browsers at one HTML file:<br/>"
                         "<a href='%1'>%1</a><br/>"
                         "Click link above to open more browser windows (if not, copy to address bar.)").arg(url.toDisplayString()));
}

/*! copy source to target file.
    If forced, if target exist, overwrite.
               if target path not exist, prepare.
    If not forced and target exits, skip copying, return false;
*/
bool MainWindow::copyTo(const QString &source, const QString &target, bool forced)
{
    if (!QFileInfo(source).exists()) {
        return false;
    }

    QFileInfo fi(target);
    if (forced) {
        if (fi.exists()) { // force to remove
            QFile f(target) ;
            f.setPermissions(QFileDevice::WriteUser | QFileDevice::WriteOwner | QFileDevice::WriteGroup | QFileDevice::WriteOwner);
            f.remove();
        } else { // prepare path
            QString path = fi.path();
            if (!QFileInfo(path).exists()) {
                QDir dir;
                if (!dir.mkpath(path)) {
                    return false;
                }
            }
        }
    }  else {
        if (fi.exists()) {
            return false;
        }
    }

    if (!QFile::copy(source, target)) {
        return false;
    }
    QFile(target).setPermissions(QFileDevice::WriteUser | QFileDevice::WriteOwner | QFileDevice::WriteGroup | QFileDevice::WriteOther);
    return true;
}

void MainWindow::anchorClicked(const QUrl &url)
{
    QDesktopServices::openUrl(url);
}

void MainWindow::clientChanged(int index)
{
    if (index >=  0 && index < m_transports.size()) {
        m_currentTransport = m_transports[index];
    }
}

// handle incoming socket
void MainWindow::newConnection()
{
    QWebSocket *socket = m_server->nextPendingConnection();
    // Returns the next pending connection as a connected QWebSocket object.
    // QWebSocketServer does not take ownership of the returned QWebSocket object.
    // It is up to the caller to delete the object explicitly when it will no longer be used, otherwise a memory leak will occur.
    // In this case, ~WebSocketTransport will delete later
    WebSocketTransport *t = new WebSocketTransport(socket);
    m_transports << t;
    m_sockets << socket;
    m_currentTransport = t;
    m_clients->clear();
    QStringList list;
    for (int i = 0; i < m_transports.size(); ++ i) {
        list << QString::number(i);
    }
    m_clients->addItems(list);
    m_clients->setCurrentIndex(list.size()-1);

    m_channel->connectTo(t);
    connect(t, SIGNAL(messageReceived(QJsonObject,QWebChannelAbstractTransport*)),
            this, SLOT(messageReceived(QJsonObject,QWebChannelAbstractTransport*)));
}

void MainWindow::sendMessage()
{
    QString text = m_lineEdit->text();
    if (text.isEmpty()) {
        return;
    }

    //emit m_jsHelper->sendText(text);

    if (m_currentTransport) {
        QWebSocket *currentSocket = m_currentTransport->socket();
        int idx = m_sockets.indexOf(currentSocket);
        if (idx != -1) {
            m_currentTransport->sendPlainTextMessage(text);
            m_lineEdit->clear();
            displayMessage(QString("Sent message to %1: ").arg(m_clients->currentText()) + text);
        } else {
            QMessageBox::information(this, __FUNCTION__, "Client gone!");
        }
    }
}

void MainWindow::messageReceived(const QJsonObject &message, QWebChannelAbstractTransport *transport)
{
    m_currentTransport = (WebSocketTransport  *)(transport);
    int i = m_transports.indexOf(m_currentTransport);
    m_clients->setCurrentIndex(i);

    QJsonDocument doc(message);
    QString s = QString(doc.toJson(QJsonDocument::Compact));
    if (message.contains("args")) {
        QJsonValue v = message["args"];
        if (v.isArray()) {
            QJsonArray a = v.toArray();
            QString t;
            for (const auto &i: a) {
                if (i.isString()) {
                    t += i.toString();
                }
            }
            if (!t.isEmpty()) {
                s = QString("Received message from %1: ").arg(m_clients->currentText()) + t;
            }
        }
    }
    displayMessage(s);
}
