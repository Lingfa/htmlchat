#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QWebSocket>

#include "WebSocketTransport.h"

/*!
    Construct the transport object and wrap the given socket.

    The socket is also set as the parent of the transport object.
*/
WebSocketTransport::WebSocketTransport(QWebSocket *socket)
    : QWebChannelAbstractTransport(socket)
    , m_socket(socket)
{
    connect(socket, SIGNAL(textMessageReceived(QString)), this, SLOT(textMessageReceived(QString)));
    connect(socket, SIGNAL(disconnected()), this, SLOT(deleteLater()));
}

/*!
    Destroys the WebSocketTransport.
*/
WebSocketTransport::~WebSocketTransport()
{
    if (m_socket->isValid()) {
        m_socket->close();
        m_socket->deleteLater();
    }
}

/*!
    Serialize the JSON message and send it as a text message via the WebSocket to the HTML/JS client.
    message example: {\"args\":[\"xxx\"],\"object\":\"core\",\"signal\":5,\"type\":1}
*/
void WebSocketTransport::sendMessage(const QJsonObject &message)
{
    QJsonDocument doc(message);
    QString s = QString::fromUtf8(doc.toJson(QJsonDocument::Compact));
    qint64 bytes = m_socket->sendTextMessage(s);
    qDebug() << bytes << ":" <<  s;
}

void WebSocketTransport::sendPlainTextMessage(const QString &text)
{
    QString json = QString("{\"args\":[\"%1\"],\"object\":\"core\",\"signal\":5,\"type\":1}").arg(text);
    QJsonParseError error;
    QJsonDocument message = QJsonDocument::fromJson(json.toUtf8(), &error);
    if (error.error) {
        qWarning() << "Failed to parse text message as JSON object:" << text
                   << "Error is:" << error.errorString();
        return;
    } else if (!message.isObject()) {
        qWarning() << "Received JSON message that is not an object: " << text;
        return;
    }

    sendMessage(message.object());
}

bool WebSocketTransport::isValid() const
{
    if (!m_socket) return false;
    return m_socket->isValid();
}

/*!
    Deserialize the stringified JSON messageData and emit messageReceived.
*/
void WebSocketTransport::textMessageReceived(const QString &messageData)
{
    QJsonParseError error;
    QJsonDocument message = QJsonDocument::fromJson(messageData.toUtf8(), &error);
    if (error.error) {
        qWarning() << "Failed to parse text message as JSON object:" << messageData
                   << "Error is:" << error.errorString();
        return;
    } else if (!message.isObject()) {
        qWarning() << "Received JSON message that is not an object: " << messageData;
        return;
    }
    emit messageReceived(message.object(), this);
}
