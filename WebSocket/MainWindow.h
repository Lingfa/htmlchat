#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QTextBrowser;
class QLineEdit;
class QComboBox;
class QWebSocketServer;
class QWebChannel;
class WebSocketTransport;
class QWebChannelAbstractTransport;
class QWebSocket;

class JsHelper;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void displayMessage(const QString &message);
private slots:
    void newConnection();
    void sendMessage();
    void messageReceived(const QJsonObject &message, QWebChannelAbstractTransport *transport);
    void anchorClicked(const QUrl &url);
    void clientChanged(int index);
private:
    QTextBrowser *m_browser;
    QLineEdit *m_lineEdit;

    QWebSocketServer *m_server;
    QWebChannel *m_channel;
    void createBar();
    void createWebSocket();
    JsHelper *m_jsHelper;
    static bool copyTo(const QString &source, const QString &target, bool forced = false);

    QList<WebSocketTransport *> m_transports;
    QList<QWebSocket *> m_sockets;
    WebSocketTransport *m_currentTransport;
    QComboBox *m_clients;
};

#endif // MAINWINDOW_H
