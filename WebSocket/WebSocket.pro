#-------------------------------------------------
#
# Project created by QtCreator 2018-07-13T01:10:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets websockets webchannel

TARGET = WebSocket
TEMPLATE = app


DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += "BUILD_DIR=\"\\\""$$OUT_PWD"\\\"\""

SOURCES += \
        main.cpp \
        MainWindow.cpp \
    JsHelper.cpp \
    WebSocketTransport.cpp

HEADERS += \
        MainWindow.h \
    JsHelper.h \
    WebSocketTransport.h

RESOURCES += \
    webchannel.qrc
